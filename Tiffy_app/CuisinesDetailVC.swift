//
//  CuisinesDetailVC.swift
//  Tiffy_app
//
// 
//

import UIKit
//import HCSStarRatingView

class CuisinesDetailVC: UIViewController {
    
    var indexPathToDisplay: IndexPath?
    @IBOutlet weak var imgCuisin: UIImageView!
    @IBOutlet weak var lblCuisinName: UILabel!
    @IBOutlet weak var lblCuisinDescription: UILabel!
   // @IBOutlet weak var viewRating: HCSStarRatingView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if indexPathToDisplay != nil {
            print("selected section=\(indexPathToDisplay?.section ?? 0) and row=\(indexPathToDisplay?.row ?? 0)")
        }
        
        self.title = constants.cuisinsList[(indexPathToDisplay?.section)!]["cuisin"] as? String ?? ""
        
        self.updateLayout()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        var detailData = constants.cuisinsList[(self.indexPathToDisplay?.section)!]["dishes"] as! [[String : Any]]
       // detailData[(self.indexPathToDisplay?.row)!] ["rating"] = self.viewRating.value
        constants.cuisinsList[(self.indexPathToDisplay?.section)!]["dishes"] = detailData
    }
    
    func updateLayout() {
        DispatchQueue.main.async {
            self.imgCuisin.image = (constants.cuisinsList[(self.indexPathToDisplay?.section)!]["dishes"] as! [[String : Any]])[(self.indexPathToDisplay?.row)!]["image"] as? UIImage
            self.lblCuisinName.text = (constants.cuisinsList[(self.indexPathToDisplay?.section)!]["dishes"] as! [[String : Any]])[(self.indexPathToDisplay?.row)!]["name"] as? String ?? ""
            self.lblCuisinDescription.text = (constants.cuisinsList[(self.indexPathToDisplay?.section)!]["dishes"] as! [[String : Any]])[(self.indexPathToDisplay?.row)!]["description"] as? String ?? ""
            //self.viewRating.value = (constants.cuisinsList[(self.indexPathToDisplay?.section)!]["dishes"] as! [[String : Any]])[(self.indexPathToDisplay?.row)!]["rating"] as? CGFloat ?? 0.0
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
