//
//  ViewController.swift
//  Tiffy_app
//
//  Created by on 31/5/19.
//  Copyright © 2019 . All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    let arrCuisin = ["Asian","MiddleEastern"]
    let dicCuisine = ["Asian":["Ginger/tomato Salad","Eggplant with minced pork","Jawain Butter Parantha"],"MiddleEastern":["Falafel","Kebab karaz","Coconut rice"]];
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Tiffy's Corner"

        
       
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //TableView Datasource and delegate methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return constants.cuisinsList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return constants.cuisinsList[section]["cuisin"] as? String ?? ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (constants.cuisinsList[section]["dishes"] as! [[String : Any]]).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        cell.imageView?.image = (constants.cuisinsList[indexPath.section]["dishes"] as! [[String : Any]])[indexPath.row]["image"] as? UIImage
        cell.textLabel?.text = (constants.cuisinsList[indexPath.section]["dishes"] as! [[String : Any]])[indexPath.row]["name"] as? String ?? ""
        cell.detailTextLabel?.text = (constants.cuisinsList[indexPath.section]["dishes"] as! [[String : Any]])[indexPath.row]["brief"] as? String ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "detailSegue", sender: indexPath)
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int{
//        return dicCuisine.count;
//    }
//     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0{
//            return dicCuisine["Asian"]?.count ?? 0
//
//        }
//        if section == 1{
//            return dicCuisine["MiddleEastern"]?.count ?? 0
//        }
//        return 0;
//    }
//
//     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//
//        return arrCuisin[section];
//    }
//
//    // Provide a cell object for each row.
//     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        // Fetch a cell of the appropriate type.
//        let cell = tableView.dequeueReusableCell(withIdentifier: "tblIdentifier", for: indexPath)
//
//        // Configure the cell’s contents.
//        let arr = arrCuisin[indexPath.section];
//        let arrvalue = dicCuisine[arr];
//        let value = arrvalue?[indexPath.row]
//        cell.textLabel?.text = value;
//
//        return cell
//    }
   
    
    //MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue" {
            if let selectedIndex = sender as? IndexPath {
                let destinationVC = segue.destination as! CuisinesDetailVC
                destinationVC.indexPathToDisplay = selectedIndex
            }
        }
    }

}

